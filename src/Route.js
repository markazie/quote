import React, { useState, useEffect } from 'react'
import { Router, Route, Redirect } from "react-router"
import App from './App'
import Time from './TimeValue'

export default function Routes() {

    const [loggedIn, setLoggedIn] = useState(null)
    useEffect(() => {
setTimeout(() => setLoggedIn(true), 10000)
    },[])

  return (
      <div>
          <Router>

      <Route
        exact
        path="/"
        render={() =>
          loggedIn ? <Redirect to="/dashboard" component={Time} /> : <App />
        }
      />
          </Router>
    </div>
  )
}
