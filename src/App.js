import "bootstrap/dist/css/bootstrap.min.css"
import React, { useEffect, useState } from "react"
import { fadeIn, bounce } from "react-animations"
import styled, { keyframes } from "styled-components"
import "./App.css"
import Author from "./Author.json"
import logo from "./logo.svg"

function App() {
  const Fade = styled.div`
    animation: 2s ${keyframes`${fadeIn}`} infinite;
  `
  const Bounce = styled.div`
    animation: 2s ${keyframes`${bounce}`} infinite;
  `

  let slice = Math.floor(Math.random() * Author.length)
  let searchInput = React.createRef()

  const [authors, setAuthors] = useState(Author.slice(slice - 3, slice))
  const [isLoading, setIsLoading] = useState(true)
  const [quotes, setQuotes] = useState([])
  const [author, setAuthor] = useState([])
  const [answer, setAnswer] = useState({ select: false })

  useEffect(() => {
    fetch(
      "https://cors-anywhere.herokuapp.com/https://mohammad-quote-server.glitch.me/quotes/random"
    )
      .then(res => res.json())
      .then(data => {
              setAuthor(author => data.author)
        setQuotes(quotes => data.quote)
        setAuthors(authors => {
          const index = Math.floor(Math.random() * authors.length + 1)
          return [
            ...authors.slice(0, index),
            data.author,
            ...authors.slice(index)
          ]
        })
      })
      .then(() => setIsLoading(false))
  }, [])

  const click = a =>
    a === author
      ? setAnswer({ select: true, correct: true })
      : setAnswer({ select: true, correct: false })

  const search = () => {
    setIsLoading(true)
    fetch(
      `https://cors-anywhere.herokuapp.com/https://mohammad-quote-server.glitch.me/quotes/search?term=${searchInput.current.value.trim()}`
    )
      .then(res => res.json())
      .then(data => {
        if (data.length === 1)
          { setAuthor(data[0].author)
    setQuotes(data[0].quote)
    setAuthors(authors => {
      const index = Math.floor(Math.random() * authors.length + 1)
      return [
        ...authors.slice(0, index),
        data[0].author,
        ...authors.slice(index)
      ]
    })
    setIsLoading(false)
  } else setIsLoading(true)
      })  
  }

  return (
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-dark indigo mb-4">
        <a className="navbar-brand" href="/">
          A day A quote
        </a>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <form className="form-inline ml-auto">
            <div className="form my-0">
              <input
                className="form-control"
                type="text"
                placeholder="Search"
                aria-label="Search"
                ref={searchInput}
                required
              />
            </div>
            <button
              href="#!"
              className="btn btn-success my-0 ml-sm-2"
              type="button"
              onClick={() => search()}
            >
              Search
            </button>
          </form>
        </div>
      </nav>

      <header className="App-header">
        {isLoading ? (
          <img className="App-logo" src={logo} alt="loading" />
        ) : (
          <div className="mt-5">
            <p>{quotes}</p>
            <h5 className="text-primary mt-5 text-warning">
              Guess, who is the author?
            </h5>
            {[...new Set(authors)].map(a => (
              <button
                key={a}
                className="btn btn-info mr-1"
                onClick={() => click(a)}
              >
                {a}
              </button>
            ))} 
          </div>
        )}
        {answer.select &&
          (answer.correct ? (
          <Bounce>

            <p className="text-success mt-5">Congratulations, that's true.</p>
          </Bounce>
          ) : (
            <Fade className="mt-5">
              <p className="text-danger">Ohhh No, try again!</p>
            </Fade>
          ))}
      </header>
    </div>
  )
}

export default App
